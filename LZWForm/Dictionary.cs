﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace LZW
{
    class Dictionary
    {
        private Nod[] nodVect;
        private int maxLength;
        private int length;
        public Dictionary(int nr)
        {
            maxLength = (int)Math.Pow(2, nr);
            nodVect = new Nod[maxLength];
            length = 0;
        }
        public bool isFull()
        {
            if (length == maxLength)
                return true;
            return false;
        }
        public void add(string ascii)
        {
            if (!isFull())
            {
                nodVect[length] = new Nod(length, ascii);
                length++;
                return;
            }
            throw new Exception("DictionaryFull");
        }
        public int foundAscii(string ascii)
        {
            for(int i = 0; i < length; i++)
            {
                if(nodVect[i].ascii== ascii)
                {
                    return nodVect[i].index;
                }
            }
            return -1;
        }
        public void empty()
        {
            if (isFull())
            {
                for (int i = 256; i < length; i++)
                {
                    nodVect[i] = null;
                }
                length = 256;
            }
        }
        public void init()
        {
            for(int i = 0; i < 256; i++)
            {
                add(((char)i).ToString());
            }
        }
        public string foundIndex(int index)
        {
            for (int i = 0; i < length; i++)
            {
                if (nodVect[i].index == index)
                {
                    return nodVect[i].ascii;
                }
            }
            return String.Empty;
        }
        public int getLength()
        {
            return length;
        }
        public int getMaxLength()
        {
            return maxLength;
        }
    }
}
