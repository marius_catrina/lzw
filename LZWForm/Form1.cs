﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LZW;

namespace LZWForm
{
    public partial class Form1 : Form
    {
        private codLZW cod;
        private decLZW dec;
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cod.startCod();
            cod.printSolution();
            label2.Text = "Codificare terminata";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int nr = int.Parse(textBox2.Text);
            bool empty = false;
            if (this.radioButton1.Checked)
            {
                empty = false;
            }
            else if (radioButton2.Checked)
            {
                empty = true;
            }
            cod = new codLZW(nr, empty);
            cod.setInput(textBox1.Text);
            label2.Text = "Fisier incarcat";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dec = new decLZW();
            dec.setInput();
            dec.startDec();
            dec.print();
            label2.Text = "Decodificare terminata";
        }
    }
}
