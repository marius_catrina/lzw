﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LZW
{
    class Nod
    {
        public int index;
        public string ascii;
        public Nod(int index, string ascii)
        {
            this.index = index;
            this.ascii = ascii;
        }
        
    }
}
