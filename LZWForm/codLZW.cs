﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCSD;

namespace LZW
{
    class codLZW
    {
        private Dictionary dictionary;
        private int[] solution;
        private int solutionLength;
        private string input;
        private bool empty;
        public codLZW(int nr, bool empty)
        {
            dictionary = new Dictionary(nr);
            dictionary.init();
            solution = new int[(int)Math.Pow(2, nr)];
            input = String.Empty;
            solutionLength = 0;
            this.empty = empty;
        }
        public void setInput(string input)
        {
            string[] lines = System.IO.File.ReadAllLines(input);
            foreach (string line in lines)
            {
                this.input += line;
            }
        }
        public void startCod()
        {
            if (input == String.Empty)
            {
                return;
            }
            int index = -1;
            string str = String.Empty;
            int i = 0;
            while (i < input.Length-1) 
            {
                str += input[i];
                index = dictionary.foundAscii(str);
                i++;
                while (index != -1 && i<input.Length)
                {
                    str += input[i];
                    index = dictionary.foundAscii(str);
                    i++;
                }
                i--;
                try
                {
                    dictionary.add(str);
                }
                catch(Exception e)
                {
                    if(empty== true)
                    {
                        dictionary.empty();
                    }
                }
                str = str.Substring(0, str.Length - 1);
                solution[solutionLength++] = dictionary.foundAscii(str);
                str = String.Empty;
            }
            if (i != input.Length)
            {
                str += input[input.Length - 1];
                solution[solutionLength++] = dictionary.foundAscii(str);
            }
        }
        public void printSolution()
        {
            string output = @"D:\LZWForm\LZWForm\out.LZW";
            BitWriter writer = new BitWriter(output);
            if(empty== true)
            {
                writer.Write(1, 1);
            }
            else
            {
                writer.Write(0, 1);
            }
            uint nrBits = (uint) Math.Log(dictionary.getMaxLength(), 2);
            writer.Write(nrBits,4);
            for(int i = 0; i < solutionLength; i++)
            {
                writer.Write((uint)solution[i], (int)nrBits);
            }
            for(int i = 0; i < 7; i++)
            {
                writer.Write(0, 1);
            }
            writer.Dispose();
        }
    }
}
