﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCSD;

namespace LZW
{
    class decLZW
    {
        private Dictionary dictionary;
        private string solution;
        private bool empty;
        private int[] input;
        private int inputLength;
        public decLZW()
        {
        }
        public void setInput()
        {
            BitReader reader = new BitReader(@"D:\LZWForm\LZWForm\out.LZW");
            uint n;
            n = reader.ReadNBit(1);
            if (n == 1)
            {
                empty = true;
            }
            else
            {
                empty = false;
            }
            n = reader.ReadNBit(4);
            dictionary = new Dictionary((int)n);
            dictionary.init();
            input = new int[(int)Math.Pow(2, n)];
            uint nr;
            inputLength = 0;
            while (true)
            {
                try
                {
                    nr = reader.ReadNBit(n);
                    input[inputLength++] = (int)nr;
                }
                catch(Exception e)
                {
                    break;
                }
            }
            reader.Dispose();
        }
        public void print()
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(@"D:\LZWForm\LZWForm\outDec.txt");
            file.Write(solution);
            file.Dispose();
        }
        public void startDec()
        {
            int i = 0;
            string str = String.Empty, entry=String.Empty;
            str = dictionary.foundIndex(input[i]);
            solution += str;
            i++;
            while (i < inputLength)
            {
                entry = String.Empty;
                if (dictionary.foundIndex(input[i]) != String.Empty)
                {
                    entry = dictionary.foundIndex(input[i]);
                }
                else if (input[i] == dictionary.getLength())
                {
                    entry = str + str[0];
                }
                solution += entry;
                dictionary.add(str + entry[0]);
                str = entry;
                i++;
            }
        }
    }
}
